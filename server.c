/*
    this is an server proto test 
*/

#include "src/net_lproto.h"
#include "src/net_sockets.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#if defined(_WIN32)
#include <Windows.h>
#else
#include <pthread.h>
#endif //_WIN32



typedef struct msg_head
{
    unsigned short len;
    unsigned short cmd; // 0 conv 1 info 2 cmd 3 pkt
}
msg_head;

typedef struct msg_conv
{
    msg_head     head;
    unsigned int conv;
}
msg_conv;


typedef struct msg_info
{
    msg_head    head;
    char        guid[128];
}
msg_info;

int             index = 0;
net_lproto*     cli[32];

#if defined(_WIN32)
DWORD WINAPI lproto_thread(LPVOID arg)  
#else
void* lproto_thread(void* arg)
#endif
{
    net_lproto* ctx = (net_lproto*)arg;

    lproto_loop(ctx);

	return 0;
}

static int svr_kcp_event(void *ctx, short events, const char *buff, int len)
{
	msg_conv* info;

    if (events == LPROTO_EVENT_NEW)
    {
        if (len < sizeof(msg_conv)) return -1;

        info = (msg_conv*)buff;
        if (info->head.cmd == 0)
        {
            // unsigned short sport = lproto_port(&ctx->peer);
            // char ssport[25] = {0x00};
            // itoa(sport, ssport, 10);

            // UINT32 conv = lproto_hash(info->guid, strlen(info->guid), ssport, strlen(ssport));

            // lproto_ikcp(ctx, conv);
			lproto_ikcp((net_lproto*)ctx, ntohl(info->conv), 128,128, 0, 20, 0, 1);
        }
    }
	return 0;
}

#define RESPONSE \
    "HTTP/1.1 200 OK\r\n" \
    "Content-Type: application/json; charset=UTF-8\r\n" \
    "Date: Thu, 06 Aug 2020 08:47:00 GMT\r\n\r\n"


static int svr_recv(void* ctx, const char *buff, int len)
{
    unsigned short sport = lproto_getport(&((net_lproto*)ctx)->peer);
    char ssport[25] = {0x00};
    char host[128] = {0x00};

    itoa(sport, ssport, 10);
    lproto_gethost(&((net_lproto*)ctx)->peer, host, 128);

    printf(" recv [%s]:[%s] client send msg %*.s \r\n", host, ssport, len, buff);

    return lproto_send((net_lproto*)ctx, RESPONSE, strlen(RESPONSE));
}

int svr_event(void* ctx, short events, void* arg)
{
	int i = 0;
#if defined(_WIN32)
	HANDLE    hThread;
	DWORD     thd;
#else
	pthread_t thd;
#endif //


    if (events == LPROTO_EVENT_NEW)
    {
        for (i = 0; i < 32; i++)
        {
            if (cli[i] == NULL) {
                cli[i] = (net_lproto*)ctx;   
                ((net_lproto*)ctx)->hid = i;
                break;
            }
        }
        ((net_lproto*)ctx)->func_net_event = ((net_lproto*)arg)->func_net_event;
        ((net_lproto*)ctx)->func_kcp_event = svr_kcp_event;
        ((net_lproto*)ctx)->func_recv = svr_recv;

    #if defined(_WIN32)
        hThread = CreateThread(NULL, 0,	lproto_thread, ctx, 0, &thd);
        if (hThread == INVALID_HANDLE_VALUE) {
            printf(" create thread err. !\r\n");
            lproto_free((net_lproto*)ctx);
        }
    #else
        if (pthread_create(&thd, NULL, lproto_thread, ctx) != 0) {
            printf(" create thread err. !\r\n");
            lproto_free(ctx);
        }
    #endif  //_WIN32
    }
	return 0;
}

int main(int argc, char* argv)
{
    net_lproto* svr;
	int i;

    for (i = 0; i < 32; i++)
    {
        cli[i] = NULL;
    }

    svr = lproto_create();
    svr->func_net_event = svr_event;

    lproto_accept(svr, "127.0.0.1", "8000", NET_PROTO_TCP);

    return 0;
}

